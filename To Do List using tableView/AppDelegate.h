//
//  AppDelegate.h
//  To Do List using tableView
//
//  Created by click labs 115 on 9/30/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

