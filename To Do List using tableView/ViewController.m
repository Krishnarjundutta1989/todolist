//
//  ViewController.m
//  To Do List using tableView
//
//  Created by click labs 115 on 9/30/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSMutableArray *arrayDataInTable;
}
@property (strong, nonatomic) IBOutlet UITextField *txtDo;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@property (strong, nonatomic) IBOutlet UIImageView *imgToDo;
@property (strong, nonatomic) IBOutlet UITableView *tblToDo;

@end

@implementation ViewController
@synthesize tblToDo;

- (void)viewDidLoad {
    [super viewDidLoad];
   _txtDo.hidden = TRUE;
    _btnSave.hidden = TRUE;

    arrayDataInTable = [NSMutableArray new];
    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger) numberOfSectionsInTableView: (UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrayDataInTable.count;
}
- (IBAction)saveData:(UIButton *)sender {
    if ([sender tag]==2) {
        
   
    [arrayDataInTable addObject:_txtDo.text];
    [tblToDo reloadData];
    _txtDo.hidden = FALSE;
     }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resuseCurrentTableViewCell" ];
    cell.textLabel.text = arrayDataInTable [indexPath.row];
    return cell;
    
}

-(IBAction) visabletxtfield:(UIButton *)sender{
    
    if ([sender tag] == 1) {
        
          _txtDo.hidden = FALSE;
         _btnSave.hidden = FALSE;
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
